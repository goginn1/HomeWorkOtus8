-- Создание таблицы Товар
CREATE TABLE Product (
    product_id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    category VARCHAR(100) NOT NULL,
    stock_amount INT NOT NULL
);

-- Создание таблицы Склад
CREATE TABLE Warehouse (
    warehouse_id SERIAL PRIMARY KEY,
    location VARCHAR(255) NOT NULL,
    capacity INT NOT NULL,
    manager_name VARCHAR(255) NOT NULL,
    phone_number VARCHAR(15) NOT NULL
);

-- Создание таблицы Заказы
CREATE TABLE Orders (
    order_id SERIAL PRIMARY KEY,
    product_id INT NOT NULL,
    order_date DATE NOT NULL,
    quantity INT NOT NULL,
    customer_name VARCHAR(255) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES Product(product_id)
);
