﻿using System;
using Npgsql;

class Program
{
    static void Main(string[] args)
    {
        string connectionString = "Host=localhost;Username=postgres;Password=postgres;Database=ozona";

        Console.WriteLine("Выберите опцию:");
        Console.WriteLine("1. Показать содержимое всех таблиц");
        Console.WriteLine("2. Добавить запись в таблицу");
        int choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                ShowAllTables(connectionString);
                break;
            case 2:
                AddRecordToTable(connectionString);
                break;
            default:
                Console.WriteLine("Неверная опция");
                break;
        }
    }

    static void ShowAllTables(string connectionString)
    {
        using (var connection = new NpgsqlConnection(connectionString))
        {
            connection.Open();

            // Добавляем отображение для всех таблиц
            ShowTable(connection, "SELECT * FROM Product", "Product");
            ShowTable(connection, "SELECT * FROM Warehouse", "Warehouse");
            ShowTable(connection, "SELECT * FROM Orders", "Orders");

            connection.Close();
        }
    }

    static void ShowTable(NpgsqlConnection connection, string query, string tableName)
    {
        using (var command = new NpgsqlCommand(query, connection))
        {
            using (var reader = command.ExecuteReader())
            {
                Console.WriteLine($"\n{tableName}:");
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Console.Write($"{reader.GetName(i)}: {reader[i]} ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }

    static void AddRecordToTable(string connectionString)
    {
        Console.WriteLine("Выберите таблицу для добавления записи:");
        Console.WriteLine("1. Product");
        Console.WriteLine("2. Warehouse");
        Console.WriteLine("3. Orders");
        int tableChoice = Convert.ToInt32(Console.ReadLine());

        using (var connection = new NpgsqlConnection(connectionString))
        {
            connection.Open();

            switch (tableChoice)
            {
                case 1:
                    AddProduct(connection);
                    break;
                case 2:
                    AddWarehouse(connection);
                    break;
                case 3:
                    AddOrder(connection);
                    break;
                default:
                    Console.WriteLine("Неверная опция");
                    break;
            }

            connection.Close();
        }
    }

    static void AddProduct(NpgsqlConnection connection)
    {
        Console.WriteLine("Введите имя, цену, категорию и количество на складе:");
        string name = Console.ReadLine();
        decimal price = Convert.ToDecimal(Console.ReadLine());
        string category = Console.ReadLine();
        int stockAmount = Convert.ToInt32(Console.ReadLine());

        string sql = "INSERT INTO Product (name, price, category, stock_amount) VALUES (@name, @price, @category, @stock_amount)";
        using (var command = new NpgsqlCommand(sql, connection))
        {
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@price", price);
            command.Parameters.AddWithValue("@category", category);
            command.Parameters.AddWithValue("@stock_amount", stockAmount);
            command.ExecuteNonQuery();
        }
    }

    static void AddWarehouse(NpgsqlConnection connection)
    {
        Console.WriteLine("Введите местоположение, вместимость, имя менеджера и номер телефона:");
        string location = Console.ReadLine();
        int capacity = Convert.ToInt32(Console.ReadLine());
        string managerName = Console.ReadLine();
        string phoneNumber = Console.ReadLine();

        string sql = "INSERT INTO Warehouse (location, capacity, manager_name, phone_number) VALUES (@location, @capacity, @manager_name, @phoneNumber)";
        using (var command = new NpgsqlCommand(sql, connection))
        {
            command.Parameters.AddWithValue("@location", location);
            command.Parameters.AddWithValue("@capacity", capacity);
            command.Parameters.AddWithValue("@manager_name", managerName);
            command.Parameters.AddWithValue("@phoneNumber", phoneNumber);
            command.ExecuteNonQuery();
        }
    }

    static void AddOrder(NpgsqlConnection connection)
    {
        Console.WriteLine("Введите ID продукта, дату заказа, количество и имя клиента:");
        int productId = Convert.ToInt32(Console.ReadLine());
        DateTime orderDate = Convert.ToDateTime(Console.ReadLine());
        int quantity = Convert.ToInt32(Console.ReadLine());
        string customerName = Console.ReadLine();

        string sql = "INSERT INTO Orders (product_id, order_date, quantity, customer_name) VALUES (@product_id, @order_date, @quantity, @customer_name)";
        using (var command = new NpgsqlCommand(sql, connection))
        {
            command.Parameters.AddWithValue("@product_id", productId);
            command.Parameters.AddWithValue("@order_date", orderDate);
            command.Parameters.AddWithValue("@quantity", quantity);
            command.Parameters.AddWithValue("@customer_name", customerName);
            command.ExecuteNonQuery();
        }
    }
}
